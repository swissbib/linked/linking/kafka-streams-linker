/*
 * streams application to link data
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.ConsumerRecordFactory
import org.apache.logging.log4j.LogManager
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.swissbib.SbMetadataDeserializer
import org.swissbib.SbMetadataModel
import org.swissbib.SbMetadataSerializer
import org.swissbib.linked.KafkaProperties
import org.swissbib.linked.KafkaTopology
import org.swissbib.types.EsBulkActions
import java.io.File
import java.nio.charset.Charset
import kotlin.test.assertEquals

class ResourceTests {
    private val log = LogManager.getLogger()
    private val props = KafkaProperties("resources.properties", log)
    private val testDriver = TopologyTestDriver(KafkaTopology(props.appProperties, log).buildResourceTopology(), props.kafkaProperties);

    private val resourcePath = "src/test/resources/resource"
    private fun readFile(fileName: String): String {
        return File("$resourcePath/$fileName").readText(Charset.defaultCharset())
    }

    @Test
    fun testCase1() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"),
                "301033935",
                SbMetadataModel().setData(readFile("input1.json")).setEsIndexName("sb-resources-20-20-2020").setEsBulkAction(
                    EsBulkActions.INDEX
                )
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        assertAll("test gnd contributor conversion",
            { assertEquals("301033935", output.key()) },
            { assertEquals("sb-resources-20-20-2020", output.value().esIndexName) },
            { assertEquals(EsBulkActions.INDEX, output.value().esBulkAction) },
            { assertEquals(readFile("output1.json"), output.value().data) }
        )
    }

    @Test
    fun testCase2() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"),
                "30375897X",
                SbMetadataModel().setData(readFile("input2.json")).setEsIndexName("sb-resources-20-20-2020").setEsBulkAction(
                    EsBulkActions.INDEX
                )
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        assertAll("test gnd contributor conversion",
            { assertEquals("30375897X", output.key()) },
            { assertEquals("sb-resources-20-20-2020", output.value().esIndexName) },
            { assertEquals(EsBulkActions.INDEX, output.value().esBulkAction) },
            { assertEquals(readFile("output2.json"), output.value().data) }
        )
    }

    @Test
    fun testCase3() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"),
                "204130050",
                SbMetadataModel().setData(readFile("input3.json")).setEsIndexName("sb-resources-20-20-2020").setEsBulkAction(
                    EsBulkActions.INDEX
                )
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        assertAll("test gnd contributor conversion",
            { assertEquals("204130050", output.key()) },
            { assertEquals("sb-resources-20-20-2020", output.value().esIndexName) },
            { assertEquals(EsBulkActions.INDEX, output.value().esBulkAction) },
            { assertEquals(readFile("output3.json"), output.value().data) }
        )
    }


    @Test
    fun testCase4() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"),
                "205022871",
                SbMetadataModel().setData(readFile("input4.json")).setEsIndexName("sb-resources-20-20-2020").setEsBulkAction(
                    EsBulkActions.INDEX
                )
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        assertAll("test gnd contributor conversion",
            { assertEquals("205022871", output.key()) },
            { assertEquals("sb-resources-20-20-2020", output.value().esIndexName) },
            { assertEquals(EsBulkActions.INDEX, output.value().esBulkAction) },
            { assertEquals(readFile("output4.json"), output.value().data) }
        )
    }

    @Test
    fun testCase6() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"),
                "205022871",
                SbMetadataModel().setData(readFile("input6.json")).setEsIndexName("sb-resources-20-20-2020").setEsBulkAction(
                    EsBulkActions.INDEX
                )
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        assertAll("test gnd contributor conversion",
            { assertEquals("205022871", output.key()) },
            { assertEquals("sb-resources-20-20-2020", output.value().esIndexName) },
            { assertEquals(EsBulkActions.INDEX, output.value().esBulkAction) },
            { assertEquals(readFile("output6.json"), output.value().data) }
        )
    }
}