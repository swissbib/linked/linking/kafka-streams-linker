/*
 * streams application to link data
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import com.beust.klaxon.JsonObject
import org.apache.http.HttpHost
import org.apache.logging.log4j.Logger
import org.elasticsearch.action.admin.indices.alias.get.GetAliasesRequest
import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.client.indices.GetIndexRequest
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.builder.SearchSourceBuilder
import java.net.ConnectException
import java.nio.charset.Charset
import java.text.Normalizer
import java.util.*
import kotlin.system.exitProcess

class ElasticIndex(private val properties: Properties,
                   private val log: Logger
) {
    private val elastic = connect()


    private val prefixList: List<String>
    private val prefixProviderList: List<String>

    init {
        if (properties.getProperty("app.topology") == "resources") {
            val appPrefix = properties.getProperty("app.prefix")
            if (appPrefix == null) {
                log.error("No prefix list defined in properties files 'app.prefix'.")
                exitProcess(1)
            }
            prefixList = appPrefix.split(",")

            val appPrefixProviders = properties.getProperty("app.prefix.provider")
            if (appPrefixProviders == null) {
                log.error("No prefix provider list defined in properties files 'app.prefix.provider'.")
                exitProcess(1)
            }
            prefixProviderList = appPrefixProviders.split(",")
        } else {
            prefixList = listOf()
            prefixProviderList = listOf()
        }

    }

    private fun connect(): RestHighLevelClient {
        val hosts = properties.getProperty("elastic.hosts").split(',')
        val port = properties.getProperty("elastic.port").toInt()

        val httpHosts = mutableListOf<HttpHost>()
        for (host in hosts) {
            httpHosts.add(HttpHost(host, port))
        }
        return RestHighLevelClient(
            RestClient.builder(
                *httpHosts.toTypedArray()
            )
        )
    }

    private val searchIndex = properties.getProperty("elastic.search.index")!!
    init {
        try {
            when {
                elastic.indices().exists(GetIndexRequest(searchIndex), RequestOptions.DEFAULT) ->
                    log.info("Searching for links in {}.", searchIndex)
                elastic.indices().existsAlias(GetAliasesRequest(searchIndex), RequestOptions.DEFAULT) -> {
                    log.info("Searching for links in alias {}.", searchIndex)
                }
                else -> {
                    log.error("Search index {} does not exist. Shutdown service ...", searchIndex)
                    exitProcess(1)
                }
            }
        } catch (ex: ConnectException) {
            log.error("Could not connect to elasticsearch at {}:{} because of {}.",
                properties.getProperty("elastic.host"),
                properties.getProperty("elastic.port"), ex.message)
            exitProcess(1)
        }
    }

    private val providers = loadProviders()

    fun link(jsonObject: JsonObject): JsonObject {
        val links = searchLink(getUri(jsonObject))
        if (links.second.isNotEmpty()) {
            // only replace the id if there is a valid link id.
            if (links.first.isNotEmpty()) {
                val prefix = (jsonObject["@id"] as String).substringBeforeLast("/")
                jsonObject["@id"] = prefix + "/" + links.first
            }
            jsonObject["owl:sameAs"] = links.second
        } else {
            jsonObject.remove("owl:sameAs")
        }
        return jsonObject
    }

    fun fixContributorUris(value: Pair<String, JsonObject>): Pair<String, JsonObject> {
        when (val contributors = value.second["dct:contributor"]) {
            is String -> {
                if (contributors.endsWith("###")) {
                    value.second["dct:contributor"] = contributors.replace("###", "")
                } else {
                    value.second["dct:contributor"] = generateContributorId(contributors)
                }
            }
            is List<*> -> {
                val resultList = mutableListOf<String>()
                for (i in contributors.indices) {
                    val contrib = contributors[i] as String
                    if (contrib.endsWith("###")) {
                        resultList.add(contrib.replace("###", ""))
                    } else {
                        resultList.add(generateContributorId(contrib))
                    }
                }
                value.second["dct:contributor"] = resultList
            }
        }
        return value
    }

    private fun generateContributorId(contributor: String): String {
        val identifier = contributor.split("###")[1]
        for (j in prefixList.indices) {
            if (identifier.startsWith(prefixList[j])) {
                return resolveKey(identifier, contributor, j)
            }
        }
        return contributor.split("###")[0]
    }

    private fun resolveKey(identifier: String, contributors: String, i: Int): String {
        val id = identifier.replace(prefixList[i], "")
        val query = QueryBuilders.termQuery("identifiers.${prefixProviderList[i]}", id)
        val searchQuery = SearchSourceBuilder.searchSource().query(query)
        val request = SearchRequest(searchIndex).source(searchQuery)
        val response = elastic.search(request, RequestOptions.DEFAULT)
        return when (response.hits.totalHits.value) {
            0L -> {
                log.error("Found no match for identifier $id in field ${prefixProviderList[i]} in index $searchIndex.")
                contributors.split("###")[0]
            }
            1L -> {
                val hit = response.hits.hits[0]
                val recovereId = hit.id!!
                val key = generateId(providers.getIdentifier(recovereId))
                contributors.split("###")[0].substringBeforeLast("/") + "/" + key
            }
            else -> {
                log.error("Found more than one result for identifier $id. Clean the index!")
                contributors.split("###")[0]
            }
        }
    }

    private fun getUri(jsonObject: JsonObject): String {
        return when (val uri = jsonObject["owl:sameAs"]) {
            is String -> uri
            is List<*> -> uri[0] as String
            else -> throw Exception("Could not parse uri from field owl:sameAs!")
        }
    }

    private fun searchLink(uri: String): Pair<String, List<String>> {
        val result = mutableSetOf<String>()
        var key = ""
        try {
            val provider = providers.getProvider(uri)
            val identifier = provider.getIdentifier(uri)
            val query = QueryBuilders.termQuery("identifiers.${provider.providerSlug}", identifier)
            val searchQuery = SearchSourceBuilder.searchSource().query(query)
            val request = SearchRequest(searchIndex).source(searchQuery)
            val response = elastic.search(request, RequestOptions.DEFAULT)
            when {
                response.hits.totalHits.value == 0L -> {
                    log.error("Found no match for identifier $identifier in field ${provider.providerSlug} in index $searchIndex.")
                }
                response.hits.totalHits.value == 1L -> {
                    val hit = response.hits.hits[0]
                    val id = hit.id!!
                    key = generateId(providers.getIdentifier(id))
                    val resources = hit.sourceAsMap["resources"]
                    resources as List<*>
                    for (resource in resources) {
                        resource as Map<*, *>
                        val resultUri = resource["uri"]
                        resultUri as String
                        result.add(resultUri)
                    }
                }
                response.hits.totalHits.value > 1 -> {
                    log.error("Found more than one result for identifier $identifier. Clean the index!")
                }
            }
            result.add(providers.getNormalizedUri(uri))
        } catch (ex: NoProviderFoundException) {
            log.error(ex.message)
        } catch (ex: IdentifierExtractionFailed) {
            log.error(ex.message)
        }
        return if (result.size == 0) {
            Pair(key, listOf())
        } else {
            Pair(key, result.toList())
        }
    }

    private fun generateId(name: String): String {
        //decompose unicode characters eg. é -> e´
        var normalizedName: String = if (!Normalizer.isNormalized(name, Normalizer.Form.NFD)) {
            Normalizer.normalize(name, Normalizer.Form.NFD)
        } else {
            name
        }
        //remove diacritical marks
        normalizedName = normalizedName.replace("[\\p{InCombiningDiacriticalMarks}\\p{IsLm}\\p{IsSk}]+".toRegex(), "")
        //transform to lower case characters
        normalizedName = normalizedName.toLowerCase()
        //URL generation
        val uuid = UUID.nameUUIDFromBytes(normalizedName.toByteArray(Charset.forName("UTF-8")))
        return uuid.toString()
    }
}