/*
 * streams application to link data
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import com.beust.klaxon.JsonObject
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Predicate
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataModel
import org.swissbib.types.EsBulkActions
import java.util.*
import java.util.regex.Pattern

class KafkaTopology(private val properties: Properties, log: Logger) {

    private val parser = JsonParser(log)
    private val elastic = ElasticIndex(properties, log)

    private val pattern = Pattern.compile("owl:sameAs")
    private val sink = properties.getProperty("kafka.topic.sink")
    private val unlinkedSink = properties.getProperty("kafka.topic.sink.unlinked")

    fun buildResourceTopology(): Topology {
        val builder = StreamsBuilder()
        val branched = builder
            .stream<String, SbMetadataModel>(properties.getProperty("kafka.topic.source"))
            .branch(
                Predicate { _, value -> value.esBulkAction == EsBulkActions.INDEX },
                Predicate { _, value -> value.esBulkAction == EsBulkActions.DELETE }
            )
        branched[0]
            .flatMapValues { value -> parseMessage(value) }
            .mapValues { value -> elastic.fixContributorUris(value) }
            .mapValues { value -> createModel(value) }
            .to(properties.getProperty("kafka.topic.sink"))

        branched[1].to(properties.getProperty("kafka.topic.sink"))

        return builder.build()
    }

    fun build(): Topology {
        val builder = StreamsBuilder()

        val branches = builder
            .stream<String, SbMetadataModel>(properties.getProperty("kafka.topic.source"))
            .branch(
                Predicate { _, value -> value.esBulkAction == EsBulkActions.INDEX }//,
                // Predicate { _, value -> value.esBulkAction == EsBulkActions.DELETE }
            )

        val linkable =
            branches[0]
                .branch(
                    Predicate { _, value -> pattern.matcher(value.data).find() }, // with links
                    Predicate { _, _ -> true } // no potential links
                )


        // deletes are simply forwarded
        // currently ignored as they need special treatment.
        //branches[1]
        //    .to(unlinkedSink)

        linkable[0]
            .flatMapValues { value -> parseMessage(value) }
            .mapValues { value -> Pair(value.first, elastic.link(value.second)) }
            .map { _, value -> KeyValue((value.second["@id"] as String).substringAfterLast("/"), value) }
            .mapValues { value -> createModel(value) }
            .to(sink)


        linkable[1]
            .to(unlinkedSink)

        return builder.build()
    }

    private fun parseMessage(value: SbMetadataModel): List<Pair<String, JsonObject>> {
        val result = parser.parse(value.data)
        return if (result.isEmpty()) {
            listOf()
        } else {
            listOf(Pair(value.esIndexName, result[0]))
        }
    }

    private fun createModel(value: Pair<String, JsonObject>): SbMetadataModel {
        return SbMetadataModel()
            .setData(value.second.toJsonString(false))
            .setEsBulkAction(EsBulkActions.INDEX)
            .setEsIndexName(value.first)
    }
}
