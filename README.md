# Kafka Streams: Linker

A microservice to find and create links between swissbib data and authority data from VIAF, GND, WIKIDATA &
DBPEDIA.
